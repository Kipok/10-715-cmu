import numpy as np
from numpy.lib.stride_tricks import as_strided
import unittest
import numpy.testing as npt

class TestNeuralNetwork(unittest.TestCase):
    def setUp(self):
        self.nn = NeuralNetwork()
        self.X_train, self.y_train, self.X_test, self.y_test = read_data()

    def test_gradients(self):
        w_names = ['w1', 'b1', 'b3', 'b2', 'w3', 'w2']
        for w_name in w_names:
            d_grad = diff_grad(self.nn, w_name, self.X_train[0], self.y_train[0])
            self.nn.backprop(self.X_train[0], self.y_train[0])
            grad = self.nn.__dict__['d' + w_name]
            npt.assert_allclose(d_grad, grad, rtol=1e-5, atol=1e-5)


def diff_grad(nn, w_name, X, y):
    eps = 1e-9
    l1 = nn.compute_loss(X, y)
    w = nn.__dict__[w_name]
    d_grad = np.empty(w.shape)
    for idx in np.ndindex(d_grad.shape):
        nn.__dict__[w_name][idx] += eps
        l2 = nn.compute_loss(X, y)
        nn.__dict__[w_name][idx] -= eps
        d_grad[idx] = (l2 - l1) / eps
    return d_grad


def read_data():
	with open('data/train-images-idx3-ubyte', 'rb') as fin:
		fin.read(16)
		X_train = np.fromfile(fin, dtype=np.uint8).reshape(-1, 28, 28) / 255.0

	with open('data/t10k-images-idx3-ubyte', 'rb') as fin:
		fin.read(16)
		X_test = np.fromfile(fin, dtype=np.uint8).reshape(-1, 28, 28) / 255.0

	with open('data/train-labels-idx1-ubyte', 'rb') as fin:
		fin.read(8)
		y_train = np.fromfile(fin, dtype=np.uint8)

	with open('data/t10k-labels-idx1-ubyte', 'rb') as fin:
		fin.read(8)
		y_test = np.fromfile(fin, dtype=np.uint8)

	return X_train[:3000], y_train[:3000], X_test[:500], y_test[:500]


class NeuralNetwork:
    def __init__(self):
        self.w1 = np.empty((8, 8, 4))
        for i in range(4):
            self.w1[:,:,i] = np.genfromtxt('init/conv_W_{}.csv'.format(i + 1), delimiter=',')
        self.b1 = np.genfromtxt('init/conv_B.csv', delimiter=',')
        self.w2 = np.genfromtxt('init/mlp_W.csv', delimiter=',')
        self.b2 = np.genfromtxt('init/mlp_B.csv', delimiter=',')
        self.w3 = np.genfromtxt('init/output_W.csv', delimiter=',')
        self.b3 = np.genfromtxt('init/output_B.csv', delimiter=',')
        self.st = 4

    def forward_pass(self, X, output_data=False):
        assert(X.shape == (28, 28))
        self.img = X
        X_view = as_strided(X, shape=((X.shape[0] - self.w1.shape[0]) // self.st + 1,
                                      (X.shape[1] - self.w1.shape[1]) // self.st + 1,
                                      self.w1.shape[0], self.w1.shape[1]),
                            strides=(X.strides[0] * self.st,
                                     X.strides[1] * self.st,
                                     X.strides[0],
                                     X.strides[1]))
        self.out1 = np.einsum('ijkl,klz->ijz', X_view, self.w1) + self.b1
        self.flat_out1 = self.out1.swapaxes(0, 2).swapaxes(1, 2).reshape(-1)
        self.out2 = np.tanh(self.w2.dot(self.flat_out1) + self.b2)
        self.out3 = np.exp(self.w3.dot(self.out2) + self.b3)
        self.out3 /= np.sum(self.out3)
        if output_data:
            for i in range(4):
                np.savetxt('conv_output_{}.csv'.format(i + 1), self.out1[:,:,i], delimiter=',')
            np.savetxt('mlp_output.csv', self.out2.reshape(144,1), delimiter=',')
            np.savetxt('final_output.csv', self.out3.reshape(10,1), delimiter=',')
        return self.out3

    def compute_loss(self, X, y):
        y_out = self.forward_pass(X)
        return -np.log(y_out[y])

    def backprop(self, X, y, output_data=False):
        self.forward_pass(X)
        self.ds3 = (np.arange(10) == y) - self.out3
        self.dw3 = -self.ds3[:,np.newaxis].dot(self.out2[np.newaxis,:])
        self.db3 = -self.ds3

        self.do2 = self.ds3.dot(self.w3)
        self.ds2 = self.do2 * (1 - self.out2 ** 2)
        self.dw2 = -self.ds2[:,np.newaxis].dot(self.flat_out1[np.newaxis,:])
        self.db2 = -self.ds2

        self.ds1 = self.ds2.dot(self.w2).reshape((4, 6, 6)).swapaxes(0, 1).swapaxes(1, 2)

        img_view = as_strided(self.img, shape=(self.w1.shape[0], self.w1.shape[1],
                                      self.out1.shape[0], self.out1.shape[1]),
                              strides=(self.img.strides[0],
                                       self.img.strides[1],
                                       self.img.strides[0] * self.st,
                                       self.img.strides[1] * self.st))
        self.dw1 = -np.einsum('ijkl,klz->ijz', img_view, self.ds1)
        self.db1 = -np.sum(self.ds1, axis=(0, 1))
        if output_data:
            for i in range(4):
                np.savetxt('conv_W_update_{}.csv'.format(i + 1), self.dw1[:,:,i], delimiter=',')
            np.savetxt('conv_B_update.csv'.format(i + 1), self.db1.reshape(4, 1), delimiter=',')
            np.savetxt('mlp_W_update.csv'.format(i + 1), self.dw2, delimiter=',')
            np.savetxt('mlp_B_update.csv'.format(i + 1), self.db2.reshape(144,1), delimiter=',')
            np.savetxt('output_W_update.csv'.format(i + 1), self.dw3, delimiter=',')
            np.savetxt('output_B_update.csv'.format(i + 1), self.db3.reshape(10,1), delimiter=',')

    def apply_backprop(self, X, y, l):
        self.backprop(X, y)
        w_names = ['w3', 'b3', 'w2', 'b2', 'w1', 'b1']
        for w_name in w_names:
            self.__dict__[w_name] -= l * self.__dict__['d' + w_name]

    def compute_error(self, X, y):
        acc = perpl = 0.0
        for i in range(X.shape[0]):
            y_out = self.forward_pass(X[i])
            acc += y[i] != np.argmax(y_out)
            perpl -= np.log(y_out[y[i]])
        return acc / X.shape[0], perpl / X.shape[0]

    def train(self, X_train, y_train, X_test, y_test):
        with open('error.csv', 'w') as fout:
            for i in range(X_train.shape[0]):
                if i % 100 == 0:
                    err = self.compute_error(X_test, y_test)
                    fout.write("{},{}\n".format(*err))
                    print("{},{}".format(*err))
                self.apply_backprop(X_train[i], y_train[i], 0.001)
            err = self.compute_error(X_test, y_test)
            fout.write("{},{}\n".format(*err))
            print("{},{}".format(*err))

if __name__ == '__main__':
    # unittest.main()
    X_train, y_train, X_test, y_test = read_data()
    nn = NeuralNetwork()
    nn.forward_pass(X_test[0], True)
    nn.backprop(X_train[0], y_train[0], True)
    nn.train(X_train, y_train, X_test, y_test)

