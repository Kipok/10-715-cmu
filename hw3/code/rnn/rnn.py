import numpy as np
import tensorflow as tf
import os, sys
import matplotlib.pyplot as plt

from tensorflow.models.rnn.ptb import reader

def gen_data(max_n=10, sample_size=100000):
    return '\n'.join(['a' * n + 'b' * n for n in np.random.randint(1, max_n, sample_size)])

class RNN:
    def __init__(self, num_classes, hid_size=2, batch_size=1,
                 num_steps=20, num_layers=1, lr=1e-4, save_path='models/'):
        tf.reset_default_graph()

        self.num_steps = num_steps
        self.batch_size = batch_size
        self.save_path = save_path
        self.num_classes = num_classes

        self.x = tf.placeholder(tf.int32, [batch_size, num_steps], name='input')
        self.y = tf.placeholder(tf.int32, [batch_size, num_steps], name='labels')

        rnn_inputs = tf.one_hot(self.x, num_classes)

        cell = tf.nn.rnn_cell.LSTMCell(hid_size, state_is_tuple=True)

        self.init_state = cell.zero_state(batch_size, tf.float32)
        hid_states, self.final_state = tf.nn.dynamic_rnn(cell, rnn_inputs, initial_state=self.init_state)

        with tf.variable_scope('output'):
            w = tf.get_variable('w', [hid_size, num_classes])
            b = tf.get_variable('b', [num_classes], initializer=tf.constant_initializer(0.0))

        hid_states = tf.reshape(hid_states, [-1, hid_size])
        y_reshaped = tf.reshape(self.y, [-1])

        logits = tf.matmul(hid_states, w) + b

        self.preds = tf.nn.softmax(logits)

        self.total_loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits, y_reshaped))
        self.train_step = tf.train.AdamOptimizer(lr).minimize(self.total_loss)

        self.saver = tf.train.Saver(max_to_keep=200)

    def gen_epochs(self, data, num_epochs):
        for i in range(num_epochs):
            yield reader.ptb_iterator(data, self.batch_size, self.num_steps)

    def train(self, data, num_epochs=5, checkpoint=None):
        tf.set_random_seed(42)
        with tf.Session() as sess:
            sess.run(tf.initialize_all_variables())
            if checkpoint is not None:
                self.saver.restore(sess, checkpoint)
            training_losses = []
            for idx, epoch in enumerate(self.gen_epochs(data, num_epochs)):
                training_loss = 0
                steps = 0
                training_state = None
                for xc, yc in epoch:
                    steps += 1
                    feed_dict={self.x: xc, self.y: yc}
                    if training_state is not None:
                        feed_dict[self.init_state] = training_state
                    loss, training_state, _ = sess.run([self.total_loss, self.final_state, self.train_step], feed_dict)
                    training_loss += loss

                print("Average training loss for Epoch {}: {}".format(idx, training_loss/steps))
                sys.stdout.flush()
                training_losses.append(training_loss/steps)
                self.saver.save(sess, os.path.join(self.save_path, 'rnn-{}.ckpt'.format(idx)), )
        return training_losses

    def gen_sample(self, checkpoint, char_to_label, label_to_char, num_chars=500):
        with tf.Session() as sess:
            sess.run(tf.initialize_all_variables())
            self.saver.restore(sess, checkpoint)
            state = None
            cur_char = char_to_label['a']
            chars = [cur_char]
            hid_states = []
            predss = []

            for i in range(num_chars):
                if state is not None:
                    feed_dict={self.x: [[cur_char]], self.init_state: state}
                else:
                    feed_dict={self.x: [[cur_char]]}

                preds, state = sess.run([self.preds, self.final_state], feed_dict)
                cur_char = np.random.choice(self.num_classes, 1, p=np.squeeze(preds))[0]
                chars.append(cur_char)
                hid_states.append(state)
                predss.append(preds)

        sample = "".join(list(map(lambda x: label_to_char[x], chars)))
        return sample, hid_states, predss



if __name__ == '__main__':
	data = gen_data()
	with open('data.txt', 'w') as fout:
		fout.write(data)
	num_classes = 3
	label_to_char = {0: 'a', 1: 'b', 2: '\n'}
	char_to_label = {'a': 0, 'b': 1, '\n': 2}
	n_data = [char_to_label[c] for c in data]
	rnn = RNN(num_classes)
	rnn.train(n_data, num_epochs=10)

	checkpoint = tf.train.latest_checkpoint(rnn.save_path)
	rnn_sample = RNN(num_classes, num_steps=1, batch_size=1)
	sample, hs, pr = rnn_sample.gen_sample(checkpoint, char_to_label, label_to_char, num_chars=10000)
	with open('sample.txt', 'w') as fout:
		fout.write(sample)

	# gen plots
	c0 = [h[0][0][0] for h in hs]
	c1 = [h[0][0][1] for h in hs]
	h0 = [h[1][0][0] for h in hs]
	h1 = [h[1][0][1] for h in hs]
	pr1 = [p[0][0] for p in pr]
	pr2 = [p[0][1] for p in pr]
	pr3 = [p[0][2] for p in pr]

	fig, axes = plt.subplots(2, 2, figsize=(18,12))
	sz = 70
	axes[0, 0].set_title('c1')
	axes[0, 0].plot(c0[:sz]);
	axes[0, 0].set_xticks(np.arange(sz));
	axes[0, 0].set_xticklabels(sample[:sz]);
	axes[0, 0].set_ylabel('value')

	axes[0, 1].set_title('c2')
	axes[0, 1].plot(c1[:sz]);
	axes[0, 1].set_xticks(np.arange(sz));
	axes[0, 1].set_xticklabels(sample[:sz]);
	axes[0, 1].set_ylabel('value')

	axes[1, 0].set_title('h1')
	axes[1, 0].plot(h0[:sz]);
	axes[1, 0].set_xticks(np.arange(sz));
	axes[1, 0].set_xticklabels(sample[:sz]);
	axes[1, 0].set_ylabel('value')

	axes[1, 1].set_title('h2')
	axes[1, 1].plot(h1[:sz]);
	axes[1, 1].set_xticks(np.arange(sz));
	axes[1, 1].set_xticklabels(sample[:sz]);
	axes[1, 1].set_ylabel('value')
	plt.show()

	fig, axes = plt.subplots(figsize=(16,5))
	plt.title('probabilities')
	plt.plot(pr1[:sz], label="'a' prob");
	plt.plot(pr2[:sz], label="'b' prob");
	plt.plot(pr3[:sz], label="'\\n' prob");
	plt.xticks(np.arange(sz), sample[:sz])
	plt.ylim([-0.02, 1.02])
	plt.ylabel('value')
	plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	plt.show()
