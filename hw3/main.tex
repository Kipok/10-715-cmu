\documentclass{article}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hwext}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{bbm}
\usepackage{float}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{mdwlist}
\usepackage{dirtree}
\usepackage{mdframed}
\usepackage[colorlinks=true]{hyperref}
\usepackage{geometry}
\geometry{margin=1in}
\geometry{headheight=2in}
\geometry{top=2in}
\usepackage{palatino}
%\renewcommand{\rmdefault}{palatino}
\usepackage{fancyhdr}
%\pagestyle{fancy}
\rhead{}
\lhead{}
\chead{%
  {\vbox{%
      \vspace{2mm}
      \large
      Machine Learning 10-715 \hfill
      Sep 21, 2016 \hfill \\
      \url{http://www.AdvIntroTo.ML} \hfill
      due on Oct 10, 2016
\\
      Carnegie Mellon University
      \\[2mm]
      Homework 3
    }
  }
}


\usepackage{paralist}

\usepackage{todonotes}
\setlength{\marginparwidth}{2.15cm}
\setlength{\parindent}{0pt}


\usepackage{tikz}
\usetikzlibrary{positioning,shapes,snakes,backgrounds}

\begin{document}
\pagestyle{fancy}

\section{Playground: Man or Machine (17 pts; Manzil)}
\begin{enumerate}
\item \textbf{(7 pts)} Suppose we want to train an MLP classifier on a mobile device such as a mobile phone or a smart watch. In this situation, having a small network is desirable. Small networks, however, cannot always directly handle non trivial datasets, like the dataset 4 in the TensorFlow playground (shown in Figure~\ref{Fig:dataset}). Under such scenarios it is a good idea to invest in {\em feature engineering} by coming up with hand-crafted, carefully designed features often with help of domain experts. Your goal in this task is to carefully select input features to design the ``smallest'' MLP classifier that can achieve a test loss score less than 0.1 for dataset 4 in TensorFlow playground. Here ``smallest'' is defined as having least number of neurons in the network. Submit a screenshot after the network achieves a test loss less of than 0.1 for dataset 4.

\begin{answer}
	Looking at the data we can notice that objects on the opposite sides (meaning on top and bottom, on left and right, on left bottom corner and
	right upper corner, on left upper corner and right bottom corner) of the space have different classes. Thus it is logical that we don't want to 
	use ``simmetric'' features, which are $x_1^2, x_2^2$ and $x_1x_2$. These features can only change the final surface simmetrically which will be
	of no use to our problem. Disabling those features and enabling all others it is possible (I needed to start learning a few times from different
	starting points to achieve this) to get the network with 1 hidden layer and just 3 neurons (2 seems to be not enough because the network doesn't 
	have desired flexibility to approximate highly non-linear function).
\end{answer}
\begin{center}
	\includegraphics[width=0.9\textwidth]{1a.png}
\end{center}

\item \textbf{(10 pts)} Machine learning experts, like yourself, is a scarce resource. If the classification task can be instead be run on a datacenter having extremely cheap and large computational resources, it will no longer be worth your time to design good features. Is there a cheaper way to obtain test loss score less than 0.1 for dataset 4 \textbf{\textit{without}} selecting any special input features and just using raw data, i.e. $X_1$ and $X_2$? If yes, construct such a network in TensorFlow playground. Submit a screenshot after the network achieves a test loss less of than 0.1 for dataset 4.
\end{enumerate}

\begin{answer}
	We know that neural networks often work better if we have more neurons and more layers (if it is possible to train such a network at all). 
	So let's just have a few hidden layers and some reasonable number of neurons on each layer and let the network learn the non-linearity this way.
	Without spending too much time we can easily obtain the desired quality.
\end{answer}
\begin{center}
	\includegraphics[width=0.9\textwidth]{1b.png}
\end{center}

\newpage

\section{Convolution Neural Networks \textbf{(50 pts; Vaishnavh)}}
\begin{enumerate}
\item \textbf{(5 pts)} Let $X$ be an input of size $n \times n$, $W$ be a smaller weight matrix of size $m \times m$ and $b \in \mathbb{R}$ be a bias value. We can convolve $W$ with $X$, using a bias $b$ and an activation function $f$, to produce an output $Z$ of size $(n-m+1) \times (n-m+1)$. Let $L$ denote the loss function of the network. Assume we have computed $\frac{\partial L}{\partial Z_{ij}}$ for all $i,j \leq n-m+1$. Write the derivative $\frac{\partial L}{\partial W_{ab}}$ for $a,b \leq m$ and $\frac{\partial L}{\partial b}$.
\end{enumerate}
\begin{answer}
	\[ 
		\frac{\partial L}{\partial W_{ab}} = 
			\sum_{i=1}^{n - m + 1}\sum_{j=1}^{n - m + 1}\frac{\partial L}{\partial Z_{ij}}\frac{\partial Z_{ij}}{\partial W_{ab}} = 
			\sum_{i=1}^{n - m + 1}\sum_{j=1}^{n - m + 1}\frac{\partial L}{\partial Z_{ij}}\frac{\partial f}{\partial Z_{ij}}X_{p+i,q+j}
	\]
	\[ 
		\frac{\partial L}{\partial b} = 
			\sum_{i=1}^{n - m + 1}\sum_{j=1}^{n - m + 1}\frac{\partial L}{\partial Z_{ij}}\frac{\partial Z_{ij}}{\partial b} = 
			\sum_{i=1}^{n - m + 1}\sum_{j=1}^{n - m + 1}\frac{\partial L}{\partial Z_{ij}}\frac{\partial f}{\partial Z_{ij}}
	\]
\end{answer}

\newpage

\section{Expressive Power of Neural Networks (30 pts; Manzil)}
In this problem, use your favorite deep learning package to implement a recurrent neural network for learning a language. We will try to learn the context free grammar $\mathcal{L}=\{a^nb^n:n\in\mathbb{N}\}$.\\

For this purpose we will train an LSTM (details in Appendix) to maximize the probability of predicting the next character correctly. To elaborate, suppose we have observed the character sequence $c_0,...,c_t$, then we would like the LSTM to assign maximum  probability mass on the correct next character. Thus the LSTM will be trained to maximize 
\begin{equation}
\max_W \sum_{c \in L} \sum_{t=1}^T \log p(\hat{c}_{t+1}=c_{t+1}|c_t,...,c_0; W)
\end{equation}
where $L \subset \mathcal{L}$ is the training set. As this is a fairly simple language, use a small dimension for the LSTM state. \textbf{Please describe the network selected, along with dimensions.}


\begin{itemize}
\item Generate dataset $L$ for this language $\mathcal{L}$ and train the LSTM. The dataset can be generated by picking many values (in 100s) of $n$ (repetitions are fine) and constructing the corresponding sequence $a^nb^n$. 
\item Try to visualize the evolution of cell/hidden state of the trained LSTM over an example sequence. The easiest way could be just plotting each cell value versus each step.
\end{itemize}

\begin{answer}
	I experimented with different datasets and different LSTM architectures. The datasets consisted of 100000 examples and I iterated over them 10 
	times (I could have had smaller datasets but iterated over them more times). The datasets varied in the maximum number of $n$ (from 10: simple 
	dataset to 100: hard dataset). The bigger is the maximum possible $n$, the more information network has to ``save'' in its memory. Although the 
	final networks will probably be similar for all values of $n$ (because the logic of the network should be the same: count the number of occurences 
	of 'a' then wait for the same number of occurences of 'b') the value of 100 is actually quite hard because the network has to remember dependencies
	from the distant past during learning. All of the architectures consisted of one layer with different number of hidden units (from 2 for the
	simple dataset to 30 for the hard one). The evolution of a hidden state for a smallest network (2 hidden units) is provided below.

	For training of the network I used Adam optimizer with the learning rate of 0.0001 and default other parameters. I also set the batch size to be
	$n$ because in this case the network will always see complicated situations (the middle of a word or the end of a word) and we would not spend
	too much time learning to print 'a' after 'a' or 'b' after 'b'.
\end{answer}
\begin{center}
	\includegraphics[width=0.9\textwidth]{3plot1.png}
	\includegraphics[width=0.9\textwidth]{3plot2.png}
\end{center}

Do you find that the LSTM has learned to be a pushdown automata? Provide an example sequence from the language and the action of the trained LSTM on the sequence as a pushdown automata.

\begin{answer}
	Yes, as shown on the plots above, LSTM clearly learned to be some kind of a pushdown automata, meaning that it remembers how many values of 'a'
	and 'b' it has seen so far (which is essentially what a pushdown automata would learn with a stack). 
	
	Looking at the plots we can see that the network is actually counting with the values of c1 and c2 (it is most visible on c1 plot: it goes up
	while the network observes values of 'a' and then the same amount down for all 'b's). It determines what to output with h1 and h2 which also have
	highly periodic structure. And the plot for probabilities is also very logical: while the network outputs 'a' it also assigns some probability to
	the 'b' character because at some point 'b' has to be printed. The more values of 'a' it outputs, the more probable 'b' becomes because network
	mostly ovserved short sequences of $a^nb^n$. Then at some point 'b' is printed and it immediately changes the networks hidden state so that now it
	outputs 'b' with probability very close to 1 until exactly $n$ characters are output. Then the network moves to the final state and output newline
	character (which I use as a terminal character for a word) with probability 1 and returns to the first state. This is exactly how a pushdown 
	automata would behave for this language.
\end{answer}

In theory, what is the smallest LSTM state dimension which can learn the language? In practice, are you able to reach it always? Why?

\begin{answer}
	In theory, network needs to maintain information about 2 things. First is the number of characters 'a' and 'b' it has seen so far. And second is
	its hidden state which determines what to output. For LSTM with arbitrary activation functions it should be possible to achieve this with just 1 
	hidden neuron (because it also has memory cell which can count the number of characters). But in practice this is very difficult because training
	neural networks is a complicated process: we are only guaranteed to converge to a local minimum, not the global one. Also in practice networks
	have finite-distance memory (meaning that in the actual implementation we only propagate gradients up to some fixed number of steps back). 
	
	Another reason why learning this language in practice is difficult is that the network never learns a probability of some character to be
	equal to exactly $1$. Even in the simple case of $n = 10$ on the plot above the probability for the last symbol in a longest word to be 'b' is not 
	$1$, but $0.998$ and this is a common situation. In my implementation I output values based on sampling from the probabilities learned by the
	network which means that in the two cases out of a thousand the network is likely to make a mistake in the end of the word. When $n$ is bigger the
	network is also less sure in its probabilities and it starts making mistakes in long words (imagine a case when $n = 100$ and the probabilities
	for 'b' in the second half of a longest word are $0.99$ which means that in one of the positions the network is very likely to make a mistake).

	Taking all this into account it is not surprising that I was not able to train a network with just one neuron. Moreover, it was necessary to have
	increasing number of units to effectively learn more complicated datasets.
\end{answer}

\section{Feedback (3 pts)}
\begin{enumerate}
\item Approximately, how long did it take you to solve each question in the homework? [in hours]
\item Technically, did you find the homework too easy, just right or too hard?
\item With respect to time, did you find it too short, just right or too hard?
\end{enumerate}
\begin{answer}
	\begin{enumerate}
		\item First question: 1 hour, second question: 6 hours, third question: 20 hours
		\item Just right, although it would be helpfull to have more detailed description for the third problem.
		\item Just right
	\end{enumerate}
\end{answer}

\end{document} 
