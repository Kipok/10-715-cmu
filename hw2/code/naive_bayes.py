import numpy as np
import pandas as pd


class NaiveBayes(object):
    def __init__(self):
        pass

    def fit(self, X, y, is_discrete=[0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1]):
        # initializing some necessary parameters of the input data
        self.is_discrete = np.array(list(map(lambda x: np.unique(x).shape[0], X.T)))
        self.is_discrete *= is_discrete

        # computing priors, assuming classes are labeled from 0 to K - 1
        self.K = np.max(y) + 1
        self.class_priors = np.mean(y[:, np.newaxis] == np.arange(self.K), axis=0)

        # estimating continuous parameters
        X_cont = X[:, self.is_discrete == 0]

        # normalization
        # self.mean = X_cont.mean(axis=0)
        # self.std = X_cont.std(axis=0)
        # X_cont = (X_cont - self.mean) / self.std

        self.est_mu = np.empty((X_cont.shape[1], self.K))
        self.est_sigma = np.empty((X_cont.shape[1], self.K))
        for k in range(self.K):
            tmp_idx = (y == k)
            self.est_mu[:, k] = np.mean(X_cont[tmp_idx], axis=0)
            self.est_sigma[:, k] = np.sqrt(np.mean((X_cont[tmp_idx] - self.est_mu[:, k]) ** 2, axis=0))
        # estimating discrete parameters, assuming features
        # are taking sequential values starting from 0
        X_dscr = X[:, self.is_discrete != 0]
        self.est_probs = [None] * X_dscr.shape[1]
        dscr_ind = np.where(self.is_discrete != 0)[0]
        for i in range(X_dscr.shape[1]):
            self.est_probs[i] = np.empty((self.K, self.is_discrete[dscr_ind[i]]))
            for k in range(self.K):
                self.est_probs[i][k] = np.mean(X_dscr[y == k, i][:, np.newaxis] ==
                                               np.arange(self.is_discrete[dscr_ind[i]]), axis=0)

    def predict(self, X):
        # normalization
        # X[:, self.is_discrete == 0] = (X[:, self.is_discrete == 0] - self.mean) / self.std
        
        # computing log-posteriors
        l = np.zeros((X.shape[0], self.K))
        for k in range(self.K):
            l[:, k] += np.log(self.class_priors[k])
            dscr_cnt = 0
            cont_cnt = 0
            for i in range(X.shape[1]):
                if self.is_discrete[i] != 0:
                    l[:, k] += np.log(self.est_probs[dscr_cnt][k, X[:, i].astype(np.int)])
                    dscr_cnt += 1
                else:
                    l[:, k] -= (self.est_mu[cont_cnt, k] - X[:, i]) ** 2 / \
                               (2 * self.est_sigma[cont_cnt, k] ** 2)
                    l[:, k] -= 0.5 * np.log(2 * np.pi) + np.log(self.est_sigma[cont_cnt, k])
                    cont_cnt += 1

        # computing predictions
        y = np.argmax(l, axis=1)
        return y, l

    def output_params(self, file_name="parameters.txt"):
        with open(file_name, 'w') as fout:
            fout.write('\n'.join(list(map(lambda x: "{:.10f}".format(x),
                                          self.class_priors))) + '\n')
            for k in range(self.K):
                dscr_cnt = 0
                cont_cnt = 0
                for i in range(self.is_discrete.shape[0]):
                    if self.is_discrete[i] != 0:
                        fout.write(' '.join(list(map(lambda x: "{:.10f}".format(x),
                                                     self.est_probs[dscr_cnt][k]))) + '\n')
                        dscr_cnt += 1
                    else:
                        fout.write("{:.10f} {:.10f}\n".format(self.est_mu[cont_cnt, k],
                                                  self.est_sigma[cont_cnt, k]))
                        cont_cnt += 1


def main():
    # Reading and cleaning data. I deleted first trash line from
    # adult.test and also all the dots at the end of the lines
    names = ['age', 'workclass', 'fnlwgt', 'education', 'education-num',
             'marital-status', 'occupation', 'relationship', 'race', 'sex',
             'capital-gain', 'capital-loss', 'hours-per-week',
             'native-country', 'target']
    data_train = pd.read_csv('data/adult.data', sep=', ',header=None,
                             names=names, engine='python')
    data_test = pd.read_csv('data/adult.test', sep=', ', header=None,
                             names=names, engine='python')

    data_train = data_train[(data_train['workclass'] != '?') &
                            (data_train['occupation'] != '?') &
                            (data_train['native-country'] != '?')]
    data_test = data_test[(data_test['workclass'] != '?') &
                          (data_test['occupation'] != '?') &
                          (data_test['native-country'] != '?')]

    map_workclass = {'Private': 0, 'Self-emp-not-inc': 1, 'Self-emp-inc': 2, 'Federal-gov': 3, 
                     'Local-gov': 4, 'State-gov': 5, 'Without-pay': 6, 'Never-worked': 7}
    map_education = {'Bachelors': 0, 'Some-college': 1, '11th': 2, 'HS-grad': 3, 'Prof-school': 4, 'Assoc-acdm': 5, 
                     'Assoc-voc': 6, '9th': 7, '7th-8th': 8, '12th': 9, 'Masters': 10, '1st-4th': 11, '10th': 12, 
                     'Doctorate': 13, '5th-6th': 14, 'Preschool': 15}
    map_marital = {'Married-civ-spouse': 0, 'Divorced': 1, 'Never-married': 2, 'Separated': 3, 'Widowed': 4, 
                   'Married-spouse-absent': 5, 'Married-AF-spouse': 6}
    map_occupation = {'Tech-support': 0, 'Craft-repair': 1, 'Other-service': 2, 'Sales': 3, 'Exec-managerial': 4, 
                      'Prof-specialty': 5, 'Handlers-cleaners': 6, 'Machine-op-inspct': 7, 'Adm-clerical': 8, 
                      'Farming-fishing': 9, 'Transport-moving': 10, 'Priv-house-serv': 11, 'Protective-serv': 12, 
                      'Armed-Forces': 13}
    map_relationship = {'Wife': 0, 'Own-child': 1, 'Husband': 2,
                        'Not-in-family': 3, 'Other-relative': 4,
                        'Unmarried': 5}
    map_race = {'White': 0, 'Asian-Pac-Islander': 1,
                'Amer-Indian-Eskimo': 2, 'Other': 3, 'Black': 4}
    map_sex = {'Female': 0, 'Male': 1}
    map_country = {'United-States': 0, 'Cambodia': 1, 'England': 2, 'Puerto-Rico': 3, 'Canada': 4, 'Germany': 5, 
                   'Outlying-US(Guam-USVI-etc)': 6, 'India': 7, 'Japan': 8, 'Greece': 9, 'South': 10, 'China': 11, 
                   'Cuba': 12, 'Iran': 13, 'Honduras': 14, 'Philippines': 15, 'Italy': 16, 'Poland': 17, 'Jamaica': 18, 
                   'Vietnam': 19, 'Mexico': 20, 'Portugal': 21, 'Ireland': 22, 'France': 23, 'Dominican-Republic': 24, 
                   'Laos': 25, 'Ecuador': 26, 'Taiwan': 27, 'Haiti': 28, 'Columbia': 29, 'Hungary': 30, 'Guatemala': 31, 
                   'Nicaragua': 32, 'Scotland': 33, 'Thailand': 34, 'Yugoslavia': 35, 'El-Salvador': 36, 'Trinadad&Tobago': 37, 
                   'Peru': 38, 'Hong': 39, 'Holand-Netherlands': 40}
    target = {'<=50K': 0, '>50K': 1}
    to_replace = {'workclass': map_workclass, 'education': map_education,
                  'marital-status': map_marital, 'occupation': map_occupation,
                  'relationship': map_relationship, 'race': map_race,
                  'sex': map_sex, 'native-country': map_country,
                  'target': target}
    data_train = data_train.replace(to_replace)
    data_test = data_test.replace(to_replace)

    X_train = data_train.drop('target', axis=1).as_matrix().astype(np.float)
    y_train = data_train['target'].as_matrix()
    X_test = data_test.drop('target', axis=1).as_matrix().astype(np.float)
    y_test = data_test['target'].as_matrix()

    nb = NaiveBayes()
    nb.fit(X_train, y_train)
    y, l = nb.predict(X_test)
    print("Accuracy = {}".format(np.mean(y == y_test)))
    nb.output_params()

    with open("posterior.txt", 'w') as fout:
        fout.write('\n'.join([' '.join(list(map(lambda x: "{:.4f}".format(x), ls)))
                              for ls in l]) + '\n')
    with open("prediction.txt", 'w') as fout:
        fout.write('\n'.join(list(map(lambda x: str(int(x)), y + 1))) + '\n')

if __name__ == '__main__':
    main()
