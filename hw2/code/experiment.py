import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split

from naive_bayes import *

cors = [0, 0.7, 0.9, 0.99, 1.0]
shifts = [7, 5, 3.8, 1.3, 0.5]
fig, axes = plt.subplots(1, 5, figsize=(20, 5))
for i in range(5):
    mean = np.array([0, 0])
    cov = np.array([[1, cors[i]], [cors[i], 1]])
    is_discrete = [0, 0]
    n = 1000

    X1 = np.random.multivariate_normal(mean, cov, n)
    y1 = np.zeros((X1.shape[0],), np.int)
    X2 = np.random.multivariate_normal(mean + [0, shifts[i]], cov, n)
    y2 = np.zeros((X2.shape[0],), np.int) + 1
    axes[i].scatter(X1[:,0], X1[:,1])
    axes[i].scatter(X2[:,0], X2[:,1], c='r')
    X = np.vstack((X1, X2))
    y = np.hstack((y1, y2))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
    nb = NaiveBayes()
    nb.fit(X_train, y_train, is_discrete)
    acc = np.mean(y_test == nb.predict(X_test)[0])
    axes[i].set_title('Accuracy = {:.4f}'.format(acc))
    axes[i].set_xlabel('x1')
    axes[i].set_ylabel('x2')
plt.show()