import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def grad_1(x):
    return np.array([2 * x[0], -2 * x[1]])

def grad_2(x):
    return np.array([3 * (x[0] ** 2 - x[1] ** 2), -6 * x[0] * x[1]])

def grad_3(x):
    return np.array([
        2 * (1.5 - x[0] + x[0] * x[1]) * (x[1] - 1) + 2 * (2.25 - x[0] + x[0] * x[1] ** 2) * \
        (x[1] ** 2 - 1) + 2 * (2.625 - x[0] + x[0] * x[1] ** 3) * (x[1] ** 3 - 1),
        2 * (1.5 - x[0] + x[0] * x[1]) * (x[0]) + 2 * (2.25 - x[0] + x[0] * x[1] ** 2) * \
        (2 * x[1] * x[0]) + 2 * (2.625 - x[0] + x[0] * x[1] ** 3) * (3 * x[1] ** 2 * x[0])
    ])

def grad_4(x):
    return np.array([
        0.004 * (x[0] - 1) + 0.4 * (x[0] ** 2 - x[1]) * 2 * x[0],
        -0.4 * (x[0] ** 2 - x[1])
    ])

def vanilla(grad, x, max_iter=10000, lr=0.001):
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        x = x - lr * grad(x)
    return updates

def momentum(grad, x, max_iter=10000, lr=0.001, mm=0.9):
    v = 0
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        v = mm * v + lr * grad(x)
        x = x - v
    return updates

def nag(grad, x, max_iter=10000, lr=0.001, mm=0.9):
    v = 0
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        v_old = v
        v = mm * v + lr * grad(x)
        x = x - (1 + mm) * v + mm * v_old
    return updates

def adagrad(grad, x, max_iter=10000, lr=0.001, eps=1e-8):
    g = np.zeros(x.shape)
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        gr = grad(x)
        g += gr ** 2
        x = x - lr * gr / (np.sqrt(g + eps))
    return updates

def rmsprop(grad, x, max_iter=10000, lr=0.001, eps=1e-8, mm=0.9):
    e = grad(x) ** 2
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        gr = grad(x)
        x = x - lr * gr / np.sqrt(e + eps)
        e = mm * e + (1 - mm) * grad(x) ** 2
    return updates

def adadelta(grad, x, max_iter=10000, eps=1e-8, mm=0.9):
    eg = grad(x) ** 2
    ed = x ** 2
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        gr = grad(x)
        x_old = x.copy()
        x = x - np.sqrt(ed + eps) * gr / np.sqrt(eg + eps)
        eg = mm * eg + (1 - mm) * grad(x) ** 2
        ed = mm * ed + (1 - mm) * (x - x_old) ** 2
    return updates

def adam(grad, x, max_iter=10000, lr=0.001, beta1=0.8, beta2=0.999, eps=1e-8):
    m =  grad(x)
    v = m ** 2
    updates = np.empty((max_iter, x.shape[0]))
    for i in range(max_iter):
        updates[i] = x.copy()
        gr = grad(x)
        x_old = x.copy()
        x = x - lr * m / (1 - beta1 ** (i + 1)) / (np.sqrt(v / (1 - beta2 ** (i + 1))) + eps)
        m = beta1 * m + (1 - beta1) * grad(x)
        v = beta2 * v + (1 - beta2) * grad(x) ** 2
    return updates

def f_1(x, y):
	return x ** 2 - y ** 2

def f_2(x, y):
    return x ** 3 - 3 * x * y ** 2

def f_3(x, y):
    return (1.5 - x + x * y) ** 2 + (2.25 - x + x * y ** 2) ** 2 + (2.625 - x + x * y ** 3) ** 2

def f_4(x, y):
    return 0.002 * (1 - x) ** 2 + 0.2 * (y - x ** 2) ** 2

def plot_data(func, grad, title):
    orig = np.array([4, 1e-5])

    fig = plt.figure(figsize=(12, 5))
    ax1 = fig.add_subplot(121, projection='3d')
    plt.title(title, loc="right")

    x = np.linspace(-5, 5, 100)
    y = np.linspace(-5, 5, 100)
    x, y = np.meshgrid(x, y)
    f = func(x, y)
    ax1.plot_surface(x, y, f, rstride=4, cstride=4, color='b')

    methods = [vanilla, momentum, nag, adagrad, rmsprop, adadelta, adam]
    labels = ['Vanilla gradient descent', 'Momentum', 'Nesterov accelerated gradient', 'Adagrad', 'RMSprop',
              'Adadelta', 'Adam']
    for method, label in zip(methods, labels):
        upd = method(grad, orig)
        x = upd[:, 0]
        y = upd[:, 1]
        f = func(x, y)
        ax1.plot(x, y, f, label=label)
        ax1.set_xlim([-5.1, 5.1])
        ax1.set_ylim([-5.1, 5.1])
        ax1.set_zlim([-40, 40])
        ax1.view_init(elev=40., azim=65)

    ax2 = fig.add_subplot(122)
    x = np.linspace(-5, 5, 100)
    y = np.linspace(-5, 5, 100)
    x, y = np.meshgrid(x, y)
    f = func(x, y)
    CS = ax2.contour(x, y, f, levels=[-24, -16, -8, -4, 0, 4, 8, 16, 24])
    ax2.clabel(CS, inline=1, fontsize=10)

    for method, label in zip(methods, labels):
        upd = method(grad, orig)
        x = upd[:, 0]
        y = upd[:, 1]
        f = func(x, y)
        ax2.plot(x, y, label=label)
        ax2.set_xlim([-5.1, 5.1])
        ax2.set_ylim([-5.1, 5.1])

    plt.legend(bbox_to_anchor=(-1.2, -0.3, 2.2, 1), loc=8, ncol=4, mode="expand")
    plt.show()

if __name__ == '__main__':
	plot_data(f_1, grad_1, "Function 1")
	plot_data(f_2, grad_2, "Function 2")
	plot_data(f_3, grad_3, "Function 3")
	plot_data(f_4, grad_4, "Function 4")
