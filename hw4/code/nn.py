import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def nn_out(x, d):
    w1 = np.empty((2, 2))
    w2 = np.empty(2)
    b1 = np.empty(2)
    w1[0, 0], w1[0, 1], w1[1, 0], w1[1, 1], b1[0], b1[1], w2[0], w2[1], b2 = d
    o1 = np.tanh(x[:,0] * w1[0, 0] + x[:,1] * w1[1, 0] + b1[0])
    o2 = np.tanh(x[:,0] * w1[0, 1] + x[:,1] * w1[1, 1] + b1[1])
    return 1 / (1 + np.exp(-o1 * w2[0] - o2 * w2[1] - b2))

def entropy_loss(out, y):
    return np.mean(-y * np.log(out) - (1 - y) * np.log(1 - out))

def l2_loss(out, y):
    return np.mean((out - y) ** 2)

def gen_d():
    d1 = np.random.rand(9) - 0.5
    d1 /= np.linalg.norm(d1)
    k = np.random.rand(3) - 0.5

    d2 = np.zeros(9)
    d2[:3] = np.cross(d1[:3], k)
    d2[3:6] = np.cross(d1[3:6], k)
    d2[6:] = np.cross(d1[6:], k)
    d2 /= np.linalg.norm(d2)
    return d1, d2

def plot_data(X, y, title):
	sz = 101
	xx = np.linspace(-4, 4, sz)
	yy = np.linspace(-4, 4, sz)

	l2v_d1 = np.empty(sz)
	l2v_d2 = np.empty(sz)
	l2e_d1 = np.empty(sz)
	l2e_d2 = np.empty(sz)

	for i, (xxc, yyc) in enumerate(zip(xx, yy)):
		l2v_d1[i] = l2_loss(nn_out(X, d0 + xxc * d1), y)
		l2v_d2[i] = l2_loss(nn_out(X, d0 + yyc * d2), y)
		l2e_d1[i] = entropy_loss(nn_out(X, d0 + xxc * d1), y)
		l2e_d2[i] = entropy_loss(nn_out(X, d0 + yyc * d2), y)

	xxm, yym = np.meshgrid(np.linspace(-4, 4, sz), np.linspace(-4, 4, sz))
	l2v = np.empty((sz, sz))
	l2e = np.empty((sz, sz))

	for i in range(xxm.shape[0]):
		for j in range(xxm.shape[1]):
			l2v[i, j] = l2_loss(nn_out(X, d0 + xxm[i, j] * d1 + yym[i, j] * d2), y)
			l2e[i, j] = entropy_loss(nn_out(X, d0 + xxm[i, j] * d1 + yym[i, j] * d2), y)

	fig, axes = plt.subplots(ncols=2, nrows=2, figsize=(10, 10))
	plt.suptitle(title, y=0.93, fontsize=12)
	axes[0, 0].plot(xx, l2v_d1, label="d_1")
	axes[0, 0].plot(yy, l2v_d2, label="d_2")
	axes[0, 0].set_xlabel("k: k * d_i")
	axes[0, 0].set_ylabel("l2 loss")
	axes[0, 0].legend(loc=2)
	CS = axes[0, 1].contour(xxm, yym, l2v)
	axes[0, 1].clabel(CS, inline=1, fontsize=10)
	axes[0, 1].set_xlabel("multiplier for d_1")
	axes[0, 1].set_ylabel("multiplier for d_2")

	axes[1, 0].plot(xx, l2e_d1, label="d_1")
	axes[1, 0].plot(yy, l2e_d2, label="d_2")
	axes[1, 0].set_xlabel("multiplier for d_i")
	axes[1, 0].set_ylabel("entropy loss")
	axes[1, 0].legend(loc=2)
	CS = axes[1, 1].contour(xxm, yym, l2e)
	axes[1, 1].clabel(CS, inline=1, fontsize=10)
	axes[1, 1].set_xlabel("multiplier for d_1")
	axes[1, 1].set_ylabel("multiplier for d_2")
	plt.show()

if __name__ == '__main__':
	d1, d2 = gen_d()
	d0 = np.array([1, 1, -1, -1, -3, 3, 1, -1, 1])

	X = pd.read_csv("data/X.csv", header=None).as_matrix()
	y = pd.read_csv("data/y.csv", header=None).as_matrix().squeeze()

	plot_data(X, y, "Full data")
	indices = np.random.choice(X.shape[0], 0.5 * X.shape[0], replace=False)
	plot_data(X[indices], y[indices], "50% of the data")
	indices = np.random.choice(X.shape[0], 0.25 * X.shape[0], replace=False)
	plot_data(X[indices], y[indices], "25% of the data")
	indices = np.random.choice(X.shape[0], 0.1 * X.shape[0], replace=False)
	plot_data(X[indices], y[indices], "10% of the data")
