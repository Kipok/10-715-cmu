\documentclass{article}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hwext}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{bbm}
\usepackage{float}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{mdwlist}
\usepackage{dirtree}
\usepackage{mdframed}
\usepackage[colorlinks=true]{hyperref}
\usepackage{geometry}
\usepackage{algorithm}
\usepackage{algpseudocode}
\geometry{margin=1in}
\geometry{headheight=2in}
\geometry{top=2in}
\usepackage{palatino}
%\renewcommand{\rmdefault}{palatino}
\usepackage{fancyhdr}
%\pagestyle{fancy}
\rhead{}
\lhead{}
\chead{%
  {\vbox{%
      \vspace{2mm}
      \large
      Machine Learning 10-715 \hfill
      Oct 12, 2016 \hfill \\
      \url{http://www.AdvIntroTo.ML} \hfill
      due on Oct 26, 2016
\\
      Carnegie Mellon University
      \\[2mm]
      Homework 4
    }
  }
}


\usepackage{paralist}

\usepackage{todonotes}
\setlength{\marginparwidth}{2.15cm}
\setlength{\parindent}{0pt}


\usepackage{tikz}
\usetikzlibrary{positioning,shapes,snakes,backgrounds}

\begin{document}
\pagestyle{fancy}

\section{Sparse-Max (15 pts; Manzil)}
\begin{question}
\begin{enumerate}
\item (2 pts) State this projection in the form of a convex optimization problem.
\item (4 pts) Write down the Lagrangian and form the dual problem. Explain how a solution to the dual problem can be used to find a solution to the primal problem.
\item (4 pts) Write down the KKT optimality conditions. Using them, show that one of the Lagrange multiplier at optimality $\lambda^*$ must satisfy 
\begin{equation}
\sum_{i=1}^K (z_i - \lambda^*)_+ = 1
\end{equation}
where $a_+ = \max\{0, a\}$.\\
\item (2 pts) How sparse is this projection? That is, for a given $z\in\mathbb{R}^K$, how many entries of the projection $p^*$ are 0?
\item (3 pts) Using the conditions for optimality derived earlier, give an efficient, noniterative algorithm for finding the projection $p^*$ that computes the exact solution up to machine precision. Provide the computational complexity for your solution in big $O$ notation.

\end{enumerate}

\end{question}
\begin{answer}
	\begin{enumerate}
		\item 
		\begin{align*}
			&\min_p \frac{1}{2}(p - z)^T(p - z) \\
			\text{s.t.}\,\,\, & {\bf{1}}^Tp = 1 \\
						& -p \preceq 0\ (\text{component-wise inequality)}
		\end{align*}
		Here I added $1/2$ for simplicity of the calculations, this doesn't change the optimal point $p^*$.
		\item
		\begin{align*}
			L(p, \lambda, \mu) &= \frac{1}{2}(p - z)^T(p - z) + \lambda{\bf{1}}^Tp - \mu^T p \\
			\frac{\partial L}{\partial p} &= (p - z) + \lambda{\bf{1}} - \mu = 0 \Rightarrow 
				p = \mu - \lambda{\bf{1}} + z \Rightarrow \\ 
			g(\lambda, \mu) &= (\mu - \lambda{\bf{1}})^T(\mu - \lambda{\bf{1}}) - 
				(\mu - \lambda{\bf{1}})^T\rbr{\mu - \lambda{\bf{1}} + z} = z(\lambda{\bf{1}} - \mu)
		\end{align*}
		Hence the dual problem is to $\max_{\lambda, \mu}g(\lambda, \mu), s.t. \mu \succeq 0$. Now, the this problem the Slater conditions obviously
		holds (we can choose $p_i = 1/K$) and thus there is a strong duality. Therefore the solutions for the dual and primal problems are the same.
		In order to find the optimal point $p^*$ using $\mu^*$ and $\lambda^*$ we can use the KKT conditions stated below.
		\item
		The KKT conditions:
		\begin{align*}
			&\begin{cases}
				p^* = \mu^* - \lambda^*{\bf{1}} + z \\
				{\bf{1}}^Tp^* = 1,	-p^* \preceq 0, \mu^* \succeq 0 \\
				\mu_i^* p_i^* = 0\ \forall i=1\dots K
			\end{cases} \Rightarrow \\
			& \sum_{i=1}^K\rbr{z_i - \lambda^* + \mu_i^*} = 1 \Rightarrow \sum_{i=1}^K\rbr{z_i - \lambda^*}_+ = 1
		\end{align*}
		The last equality follows from the fact that either $z_i - \lambda^* + \mu_i^* = 0$ or $\mu_i^* = 0$ and $z_i - \lambda^* + \mu_i \ge 0 
		\ \forall i$.
		\item See the last question
		\item We know that $\sum_{i=1}^K\rbr{z_i - \lambda^*}_+ = 1$. From this equality it is actually possible to find the value for $\lambda^*$. Let's
		do it by the algorithm~\ref{algo:1} (for this algorithm we will assume without loss of generality that the values of $z_1 \ge z_2 \ge \cdots \ge
		z_K$).
	
		Indeed, at the end of the algorithm, for some $i \in \{0,\dots,K - 1\}$
			\[ \lambda^* = \frac{\sum_{j=1}^{i+1}(z_j) - 1}{i + 1} \Leftrightarrow  \sum_{j=1}^{i+1}\rbr{z_j - \lambda^*} = 1\]
	\end{enumerate}
\end{answer}
\begin{answer}
	Moreover, $\lambda^* < z_j\ \forall j: 1 <= j <= i + 1$. To prove that it is enough to show that $\lambda^* < z_{i + 1}$ because $z_{i+1} \le z_i
	\le \dots \le z_1$. But from the inequality in the while statement we know that 
	\[ \sum_{j=1}^iz_j - 1 < i z_{i + 1} \Rightarrow \lambda^* = \frac{\sum_{j=1}^{i}z_j + z_{i + 1} - 1}{i + 1} < \frac{i z_{i + 1} + z_{i + 1}}
	{i + 1} = z_{i + 1} \]
	For $i = 0$	the inequality in the while statement doesn't hold, but $\lambda^* = z_1 - 1 < z_1$.

	For all $j > i + 1$ (if there are any) $\lambda^* >= z_j$. From all that we can conclude that
	\[ \sum_{j=1}^K\rbr{z_j - \lambda^*}_+ = \sum_{j=1}^{i+1}\rbr{z_j - \lambda^*} = 1 \]
	which means that the algorithm finds the right $\lambda^*$. From that we can reconstruct the other variables: 
	\[ \mu_j = 0, p_j = z_j - \lambda^*\ \forall j \le i + 1, \mu_j = \lambda^* - z_j, p_j = 0\ \forall j > i + 1 \]
	Of course we should also rearrange the variables in the original order if $z_j$ were not descending from the beginning. It is clear that this
	algorithm complexity is $O(K)$ and that it naturally encourages sparsity. The number of zeros in $p$ can be computed using the same algorithm and
	depends on how ``dense'' is $z$.
\end{answer}
\begin{algorithm}
	\begin{algorithmic}
		\State \textbf{initialize} $\lambda^* \leftarrow z_1 - 1, i \leftarrow 1, s \leftarrow z_1$
		\While{$i < K$ and $\lambda^* < z_{i + 1} $}
			\State $\lambda^* = \frac{s + z_{i + 1} - 1}{i + 1}$
			\State $s = s + z_{i + 1}$
			\State $i = i + 1$
		\EndWhile
	\end{algorithmic}
	\label{algo:1}
	\caption{The algorithm to compute $\lambda^*$}
\end{algorithm}
\newpage

\section{Bundle Methods (10 pts; Manzil)}
\begin{question}
Show that the dual optimization problem for the case when $\Omega[w] = \frac12 \|w\|^2_2$ we need to solve in Bundle methods is of the following form \textbf{\textit{regardless of the choice of the empirical risk}} $R_{\text{emp}}[w]$!

\begin{equation}
\begin{aligned}
\min_\beta \quad & \quad \frac{1}{2\lambda} \beta^TAA^T\beta - \beta^Tb\\
s.t. \quad & \quad  \|\beta\|_1=1, \beta_i \geq 0 \text{ for } i=1,2,...,t
\end{aligned}
\end{equation}
What do $A$ and $b$ correspond to in the original formulation?

\end{question}
\begin{answer}
	Let's reformulate the original problem in the following way:
	\begin{align*}
		&\min_{u, w} u + \frac{\lambda}{2}\nbr{w}^2 \\
		\text{s.t.}\ &u \ge \inner{a_i}{w} + b_i, i=1 \dots t
	\end{align*}
	Let's compute the Lagrangian and minimize it:
	\begin{align*}
		L(\beta, u, w) &= u + \frac{\lambda}{2}\nbr{w}^2 + \sum_{i=1}^t \beta_i\rbr{\inner{a_i}{w} + b_i - u} \Rightarrow \\
		\frac{\partial L}{\partial u} &= 1 - \sum_{i=1}^t \beta_i = 0,
		\frac{\partial L}{\partial w} = \lambda w + \sum_{i=1}^t \beta_i a_i = 0 \Rightarrow w = -\frac{1}{\lambda}A^T\beta \Rightarrow \\
		g(\beta) &= \frac{1}{2\lambda}\beta^TAA^T\beta + \beta^Tb - \frac{1}{\lambda}\beta^TA\sum_{i=1}^t\beta_ia_i = 
		-\frac{1}{2\lambda}\beta^TAA^T\beta + \beta^Tb
	\end{align*}
	So the dual problem (if we replace maximum with minimum) is
	\begin{align*}
		&\min_{\beta} \frac{1}{2\lambda}\beta^TAA^T\beta - \beta^Tb \\
		\text{s.t.}\ &\beta_i \ge 0, i=1 \dots t, \sum_{i=1}^t \beta_i = 1 \\
		&A = \begin{bmatrix} a_1 \\ a_2 \\ \vdots \\ a_t \end{bmatrix} (\text{each $a_i$ is a vector-row}),
		b = \begin{bmatrix} b_1 \\ b_2 \\ \vdots \\ b_t \end{bmatrix}
	\end{align*}
\end{answer}


\section{Short Answer (2x10=20 pts; Vaishnavh)}
\paragraph{1} Define $\Delta_k := \{ \mathbf{x} \in \mathbb{R}^k | x_i \geq 0, \sum_i x_i = 1 \}$. Let $M \subset \mathbb{R}^n$ be an arbitrary set, $C \subset \mathbb{R}^n$ be a convex set and $k \in \mathbb{N}$ be an arbitrary natural number. Show that if $x_1,...,x_k \in C$ and $\theta_1,...,\theta_k \in \Delta_k$, then their convex combination $y = \sum_i \theta_i x_i$ is also in $C$. The definition of convexity holds for $k=2$, you need to prove it for any general $k > 2$.
\begin{answer}
	Let's prove it by induction. We know that it is true for $k = 2$. Suppose it is true for $k = p \Rightarrow$
	\begin{align*} 
	\sum_{i=1}^{p + 1}\theta_i x_i = \frac{\sum_{i=1}^p \theta_i}{\sum_{i=1}^p \theta_i}\sum_{i=1}^{p}\theta_i x_i + 
	(1 - \sum_{i=1}^p \theta_i)x_{p + 1} &= \sum_{i=1}^p \theta_i \sum_{i=1}^p \frac{\theta_i}{\sum_{i=1}^p \theta_i}x_i + 
	(1 - \sum_{i=1}^p\theta_i)x_{p + 1} = \\ &= \sum_{i=1}^p \theta_iy + (1 - \sum_{i=1}^p\theta_i)x_{p + 1} \in C
	\end{align*}
\end{answer}
\vspace{-3mm}
\paragraph{2} Then, define $conv_1(M)$ to be the intersection of all convex sets containing $M$ and define $conv_2(M)$ to be the set of all convex combinations of points in $M$. Use the previous proof to show that $conv_1(M) = conv_2(M)$ by showing that each set contains the other.
\begin{answer}
	conv2($M$) $\subset$ conv1($M$) because by the previous proof conv2($M$) is convex and obviously contains all points in $M$ thus it is one of the
	elements in the intersection forming conv1($M$).

	conv1($M$) $\subset$ conv2($M$) because conv2($M$) is a minimal convex set containing $M$ (if we delete any point from conv2($M$) we will lose
	convexity) thus there can't be any smaller set in the intersection and all the elements in conv1($M$) lie in the conv2($M$).
\end{answer}
\vspace{-3mm}
\paragraph{3} Is a hyperplane  $HP(a,b) = \{x \in \mathbb{R}^d | a^\top x = b \}$ a convex set? Justify. What is the distance between parallel hyperplanes $HP(a,b_1)$ and $HP(a,b_2)$?
\begin{answer}
	It is a convex set: $\forall x_1, x_2 \in HP(a, b), \alpha \in [0, 1]: a^T(\alpha x_1 + (1 - \alpha)x_2) = \alpha b + (1 - \alpha) b = b 
	\Rightarrow \alpha x_1 + (1 - \alpha)x_2 \in HP(a, b)$. The distance between two hyperplanes is $\frac{|b_2 - b_1|}{a^Ta}$. To prove that let's
	take any point $x$ on the first plane: $a^Tx = b_1$. The projection of this point on the other plane can be obtained by moving in the direction
	of $a$: $y = x + \lambda a$. But $y$ lies on the second hyperplane thus $a^T(x + \lambda a) = b_2 \Rightarrow \lambda = \frac{b_2 - b_1}{a^Ta}$
	and the distance is $|\lambda|$ which concludes the proof.
\end{answer}
\vspace{-3mm}
\paragraph{4} Is a half space $HS(a,b) = \{x \in \mathbb{R}^d | a^\top x \leq b\}$ a convex set? Justify. Under what conditions on $a_1,b_1,a_2,b_2$ does one half space $HS(a_1,b_1)$ completely contain another $HS(a_2,b_2)$?
\begin{answer}
	It is a convex set: $\forall x_1, x_2 \in HS(a, b), \alpha \in [0, 1]: a^T(\alpha x_1 + (1 - \alpha)x_2) \le \alpha b + (1 - \alpha) b = b 
	\Rightarrow \alpha x_1 + (1 - \alpha)x_2 \in HS(a, b)$. The two border hyperspaces has to be parallel (otherwise they will intersect and the two 
	half spaces will part) $\Rightarrow a_1 = a_2$. And also one has to contain the other thus $b_2 \le b_1$.
\end{answer}
\vspace{-3mm}
\paragraph{5} For any two points $u,v \in R^d$, show using the previous parts that the set of points which is closer in euclidean distance to $u$ than to $v$ is a convex set.
\begin{answer}
	We know that all the points equidistant from $u$ and $v$ lie on a hyperplane. Thus the set of points which is closer to $u$ is a half space and we
	proved that it is a convex set.
\end{answer}
\vspace{-3mm}
\paragraph{6} Suppose $f : \mathbb{R}_+ \rightarrow \mathbb{R}$ is convex. Show that $f(sx)$ is convex for any $s > 0$. Show that its running average $F(x) = \tfrac1{x} \int_0^x f(t)dt$ is convex.
\begin{answer}
	By definition if $f$ is convex $\Leftrightarrow f(\alpha x_1 + (1 - \alpha)x_2) \le \alpha f(x_1) + (1 - \alpha)f(x_2)\ \forall x_1, x_2$. So that
	holds $\forall sx_1, sx_2$ as well which proves that $f(sx)$ is convex. For the second question if we substitute $t = x\tau$ and use the convexity 
	of $f(x\tau)$:
	\begin{align*}
		\alpha F(x_1) &+ (1 - \alpha)F(x_2) = \frac{\alpha}{x_1}\int_0^{x_1} f(t)dt + \frac{1 - \alpha}{x_2}\int_0^{x_2} f(t)dt = \\ &= 
		\int_0^1 \alpha f(x_1\tau) + (1 - \alpha) f(x_2\tau) d\tau \ge \int_0^1 f\rbr{\sbr{\alpha x_1 + (1 - \alpha)x_2}\tau}d\tau = \\ &=
		\frac{1}{\alpha x_1 + (1 - \alpha)x_2}\int_0^{\alpha x_1 + (1 - \alpha)x_2} f(t)dt = F(\alpha x_1 + (1 - \alpha)x_2)
	\end{align*}
\end{answer}
\vspace{-3mm}
\paragraph{7} Convert the following LP to standard form of $\min_u c^\top u$ subject to $Au=b, u \geq 0$. $\max_{x,y,z} 3x - y + z$ subject to $-1 \leq x \leq 1,\, -1 \leq y \leq 1,\, x+y+z = 1$. What is the optimal value of this LP and attained at what point?
\begin{answer}
	Using the equality constraint we can rewrite the objective function as $2x - 2y + 1$ and because of the inequalities this can not be greater than
	$5$ which is obtained at $x = 1, y = -1, z = 1$. To write it in the standard form let's introduce new variables $u_1 = 1 - x, u_2 = x + 1, u_3 = 1
	- y, u_4 = y + 1, u_5 = 1$. Then the standard form is the following:
	\[ c^T = \begin{bmatrix} 1 & -1 & -1 & 1 & -1 \end{bmatrix}, A = \begin{bmatrix} 1 & 1 & 0 & 0 & 0 \\ 0 & 0 & 1 & 1 & 0 \\ 0 & 0 & 0 & 0 & 1
	\end{bmatrix}, b = \begin{bmatrix} 2 \\ 2 \\ 1 \end{bmatrix} \]
\end{answer}
\vspace{-3mm}
\paragraph{8} Let $S$ be the set of all minimizers of a convex function $f$.  Prove that $S$ is convex.  Now suppose that $f$ is strictly convex.  Prove that $S$ contains only one element, i.e., $f$ has a unique minimizer.  
\begin{answer}
	Let $\partial f(x)$ be a subgradient of $f$ at point $x$, then $f(x) \ge f(y) + \partial f(y)^T(x - y)\ \forall x, y$. In the minimum point 
	$\partial f(y) = 0 \Rightarrow f(y) \le f(x)\ \forall x$. Let $y_1, y_2$ be minimal points then
	\[ f(\alpha y_1 + (1 - \alpha y_2)) \le \alpha f(y_1) + (1 - \alpha)f(y_2) \le \alpha f(x) + (1 - \alpha)f(x) = f(x)\ \forall x \]
	Hence $\alpha y_1 + (1 - \alpha) y_2$ is a minimal point and $S$ is convex.

	If $f$ is strictly convex $\Rightarrow f(x) > f(y) + \partial f(y)^T(x - y)\ \forall x, y$. If there are two minimal points $y_1, y_2$ then
	$f(y_1) < f(y_2)$ and $f(y_2) < f(y_1)$ which is a contradiction.
\end{answer}
\vspace{-3mm}
\paragraph{9} What are the singular values of an $n \times n$ square orthogonal matrix? (justify in one line) Show with a short example that the set of all orthogonal matrices is non-convex.
\begin{answer}
	Singular values of an orthogonal matrix are all $1$s because SVD is $U = UII$. $I$ and $-I$ are both orthgonal but their convex combination: $0.5I
	+ 0.5(-I)$ is zero-matrix which is not orthogonal.
\end{answer}
\vspace{-3mm}
\paragraph{10} For a differentiable $\lambda$-strongly convex function, starting from the Taylor-like first order definition, prove that $\| \nabla f(y) - \nabla f(x) \|_2 \geq \lambda \|y-x\|_2$. If additionally, its gradient is $L$-Lipschitz, what can we say about $\lambda,L$?
\begin{answer}
	Using Cauchy-Bunyakovsky-Schwarz inequality:
	\begin{align*}
		&\begin{cases}
			f(x) \ge f(y) + \nabla f(y)^T(x - y) + \frac{\lambda}{2}\|x - y\|^2 \\
			f(y) \ge f(x) + \nabla f(x)^T(y - x) + \frac{\lambda}{2}\|x - y\|^2
		\end{cases} \Leftrightarrow \\
		& \Leftrightarrow \rbr{\nabla f(y) - \nabla f(x)}^T(x - y) \ge \lambda \|x - y\|^2 \Rightarrow \\
		&\Rightarrow \|\nabla f(y) - \nabla f(x)\|\|x - y\| \ge \rbr{\nabla f(y) - \nabla f(x)}^T(x - y) \ge \lambda \|x - y\|^2 \Rightarrow \\
		&\Rightarrow \|\nabla f(y) - \nabla f(x)\| \ge \lambda \|x - y\|
	\end{align*}
	If additionally, its gradient is $L$-Lipschitz, then
	\[ \|\nabla f(y) - \nabla f(x)\| \ge \lambda \|x - y\| \ge \frac{\lambda}{L}\|\nabla f(y) - \nabla f(x)\| \Rightarrow L \ge \lambda \]
\end{answer}

\newpage

\section{Convergence rate of subgradient method (15 pts; Manzil)}
\def\R{\mathbb{R}}
\begin{enumerate}
\item (3 points) Use $x^{(k)}=x^{(k-1)}-t_kg^{(k-1)}$ and the definition of a 
subgradient of $f$ at $x^{(k-1)}$ to show that
\begin{equation*}
\|x^{(k)}-x^\star\|_2^2 \leq \|x^{(k-1)}-x^\star\|_2^2 - 2t_k
\big(f(x^{(k-1)})-f(x^\star)\big) + t_k^2 \|g^{(k-1)}\|_2^2.
\end{equation*}
\begin{answer}
	\begin{align*}
		\| x^{(k)} - x^* \|^2 &= \| x^{(k - 1)} - t_k g^{(k - 1)} - x^*\|^2 \le \|x^{(k - 1)} - x^*\|^2 + t_k^2 \|g^{(k - 1)}\|^2 - 
		2t_k\langle x^{(k - 1)} - x^*, g^{(k - 1)}\rangle \le \\
		&\le \|x^{(k - 1)} - x^*\|^2 + t_k^2 \|g^{(k - 1)}\|^2 - 2t_k\rbr{f(x^{(k - 1)}) - f(x^*)}
	\end{align*}
\end{answer}

\item (3 points) Unroll the recursive inequality from part (a), use Lipschitz continuity of $f(\cdot)$, and $R=\|x^{(0)}-x^\star\|_2$, to show that
\begin{equation*}
\|x^{(k)}-x^\star\|_2^2 \leq R^2 -
2 \sum_{i=1}^k t_i \big(f(x^{(i-1)})-f(x^\star)\big) 
+ G^2 \sum_{i=1}^k t_i^2.
\end{equation*}
\begin{answer}
	Using the definition of a subgradient and Lipshitz continuity we can get
	\begin{align*}
		\forall x, k: \abr{\inner{g^{(k - 1)}}{x - x^{(k - 1)}}} \le \abr{f(x) - f(x^{(k -  1)})} \le G\nbr{x - x^{(k - 1)}}
	\end{align*}
	Let's take $x = x^{(k - 1)} + \frac{g^{(k - 1)}}{\nbr{g^{(k - 1)}}}$, then
	\[ \forall k: \abr{\inner{g^{(k - 1)}}{\frac{g^{(k - 1)}}{\nbr{g^{(k - 1)}}}}} = \nbr{g^{(k - 1)}} \le 
	G\nbr{ \frac{g^{(k - 1)}}{\nbr{g^{(k - 1)}}} } = G\]
	Now we can unroll the recursive inequality:
	\begin{align*}
		&\| x^{(k)} - x^* \|^2 \le \|x^{(k - 1)} - x^*\|^2 + t_k^2 \|g^{(k - 1)}\|^2 - 2t_k\rbr{f(x^{(k - 1)}) - f(x^*)} \le \\
		&\le \|x^{(k - 2)} - x^*\|^2 + \sum_{i=k-1}^kt_i^2 \|g^{(i - 1)}\|^2 - \sum_{i=k-1}^k2t_i\rbr{f(x^{(i - 1)}) - f(x^*)} \le \\
		&\le R^2 - 2\sum_{i=1}^k t_i\rbr{f(x^{(i - 1)}) - f(x^*)} + \sum_{i=1}^kt_i^2 \|g^{(i - 1)}\|^2 \le 
		R^2 - 2\sum_{i=1}^k t_i\rbr{f(x^{(i - 1)}) - f(x^*)} + G^2\sum_{i=1}^kt_i^2
	\end{align*}
\end{answer}

\item (3 points) Use $\|x^{(k)}-x^\star\|_2 \geq 0$, and rearrange the result of part (b)
to give an upper bound on $2 \sum_{i=1}^k t_i (f(x^{(i-1)})-f(x^\star))$.
Then use the definition of $f(x^{(k)}_\text{best})$ to conclude that
\begin{equation}
\label{eq:basicineq}
f(x^{(k)}_\text{best}) - f(x^\star) \leq
\frac{R^2 + G^2 \sum_{i=1}^k t_i^2}{2\sum_{i=1}^k t_i}.
\end{equation}
We'll call \eqref{eq:basicineq} as our basic inequality.
\begin{answer}
	\begin{align*}
		&\| x^{(k)} - x^* \|^2 \le R^2 - 2\sum_{i=1}^k t_i\rbr{f(x^{(i - 1)}) - f(x^*)} + G^2\sum_{i=1}^kt_i^2 \Leftrightarrow \\
		\Leftrightarrow\ &2\sum_{i=1}^k t_i\rbr{f(x^{(i - 1)}) - f(x^*)} \le R^2 + G^2\sum_{i=1}^kt_i^2 - \|x^{(k)} - x^*\|^2 \le R^2 +
		G^2\sum_{i=1}^kt_i^2 \\
	\end{align*}
	Now
	\begin{align*}
		&f(x_{\text{best}}^{(k)}) \le f(x^{(i - 1)})\ \forall i \Rightarrow 2\sum_{i=1}^k t_i\rbr{f(x_{\text{best}}^{(k)}) - f(x^*)} \le 
		2\sum_{i=1}^k t_i\rbr{f(x^{(i - 1)}) - f(x^*)} \Rightarrow \\
		\Rightarrow\ &f(x_{\text{best}}^{(k)}) - f(x^*) \le \frac{R^2 + G^2\sum_{i=1}^kt_i^2}{2\sum_{i=1}^k t_i}
	\end{align*}
\end{answer}

\item (2 points) Consider a constant step size $t_k=t$ for all $k=1,2,3,\ldots$.
Plug this into in \eqref{eq:basicineq}, and take the limit as
$k\rightarrow\infty$. What do you conclude?
\begin{answer}
	\begin{align*}
		f(x_{\text{best}}^{(k)}) - f(x^*) \le \frac{R^2 + G^2kt^2}{2kt} = \frac{R^2}{2kt} + \frac{tG^2}{2} \xrightarrow[k \to \infty]{} \frac{tG^2}{2}
	\end{align*}
	This means that according to this upper bound the algorithm might not converge because the upper bound doesn't converge to zero. But theoretically
	there might be a better upper bound which converges to zero so we can't really conclude much.
\end{answer}

\item (2 points) Consider a sequence of step sizes satisfying
\begin{equation*}
\sum_{i=1}^\infty t_i^2 < \infty \;\;\;\text{and}\;\;\;
\sum_{i=1}^\infty t_i = \infty.
\end{equation*}
Can you provide an example of such a sequence. For such step sizes, take the limit as $k\rightarrow\infty$ in the
basic inequality \eqref{eq:basicineq}. Now what do you conclude?
\begin{answer}
	The example of such a sequence is $t_k = \frac{1}{k}$. Let's plug it into the ineqality:
	\[ f(x_{\text{best}}^{(k)}) - f(x^*) \le \frac{R^2 + G^2\sum_{i=1}^k\frac{1}{i^2}}{2\sum_{i=1}^k \frac{1}{i}} = 
	\frac{R^2}{2\sum_{i=1}^k\frac{1}{i}} + \frac{G^2\sum_{i=1}^k\frac{1}{i^2}}{2\sum_{i=1}^k \frac{1}{i}}  \xrightarrow[k \to \infty]{} 0\]
	In this case the algorithm is guaranteed to converge.
\end{answer}

\item (2 points) Fix the some number of iterations $k$. Let, the choice of step size be $t_i=c$ for all $i=1,\ldots k$. Find $c$ which minimizes the bound on the right-hand side 
of \eqref{eq:basicineq}. Hence, from this, what can we say about the best provable convergence rate of the subgradient method?
\begin{answer}
	\[ \rbr{\frac{R^2}{2kc} + \frac{cG^2}{2}}' = -\frac{R^2}{2kc^2} + \frac{G^2}{2} = 0 \Rightarrow c = \frac{R}{2G\sqrt{k}} \Rightarrow
		f(x_{\text{best}}^{(k)}) - f(x^*) \le \frac{RG}{\sqrt{k}}\]
	So by choosing $c$ that way is is guranteed that the convergence rate is not worse than $O(\frac{1}{\sqrt{k}})$
\end{answer}

\end{enumerate}

\newpage

\section{Gradient Descent (20 pts; Vaishnavh)}
\begin{question}
Evaluate each of these gradient update strategies on following functions starting from the initial point (0, 0):
\begin{enumerate}
\item (5 pts) $f_1(x,y) = x^2 - y^2$
\item (5 pts) $f_2(x,y) = x^3 - 3xy^2$
\item (5 pts) $f_3(x,y) = \left(1.5-x+xy\right)^{2}+\left(2.25-x+xy^{2}\right)^{2} +\left(2.625-x+xy^{3}\right)^{2}$
\item (5 pts) $f_4(x,y) = 0.002(1 - x)^2 + 0.2(y - x^2)^2$
\end{enumerate}
Plot the trajectory on top of the contour of the function.
\end{question}
\begin{answer}
	The plots for the functions in question are provided below. We can see that mostly adaptive methods generally work better than non-adaptive (the
	ones that only consider the last gradient) because if the gradient at the current point is too big they would scale it and still make a small step
	and if it is too small they would enlarge it. I would say that Adam
	works better than all these methods in all these cases so one conclusion is to use Adam if possible.
\end{answer}
\begin{center}
	\includegraphics[width=0.9\textwidth]{func1.png}
	\includegraphics[width=0.9\textwidth]{func2.png}
	\includegraphics[width=0.9\textwidth]{func3.png}
	\includegraphics[width=0.9\textwidth]{func4.png}
\end{center}

\newpage

\section{Non-convexity (20 pts; Manzil)}
\begin{question}
\begin{enumerate}
\item (2 pts) Write down a optimization problem for training this neural network with (a) binary cross-entropy loss and (b) mean-square error loss.
\item (2 pts) The parameter space is $\mathbb{R}^9$ which is not possible to visualize directly. So randomly generate two orthogonal vectors $d_1, d_2 \in \mathbb{R}^9$. Normalize the vectors such that $\|d_i\|=1$. (Generate them randomly, do not use standard basis vectors.)
\item (4 pts) Draw the following the plots:
\begin{itemize}
\item Plot of the objective function using binary cross-entropy versus the direction $d_1$.
\item Plot of the objective function using mean-square error loss versus the direction $d_1$.
\item Plot of the objective function using binary cross-entropy versus the direction $d_2$.
\item Plot of the objective function using mean-square error loss versus the direction $d_2$.
\item Contour plot of the objective function using binary cross-entropy in the directions of the two orthogonal vectors $d_1, d_2$.
\item Contour plot of the objective function using mean-square error in the directions of the two orthogonal vectors $d_1, d_2$.
\end{itemize} 
\item (4 pts) Repeat part (3), but using only the first 50\% of the data provided in folder.
\item (4 pts) Repeat part (3), but using only the first 25\% of the data provided in folder.
\item (4 pts) Repeat part (3), but using only the first 10\% of the data provided in folder.
\end{enumerate}
\end{question}
For the plots you should vary the parameters from $-4d_i$ to $4d_i$.

\begin{answer}
	Let's denote
	\begin{align*}
		o_1(x_1, x_2) &= \tanh\rbr{x_1w_{11}^{(1)} + x_2w_{21}^{(1)} + b_1^{(1)}} \\
		o_2(x_1, x_2) &= \tanh\rbr{x_1w_{12}^{(1)} + x_2w_{22}^{(1)} + b_2^{(1)}} \\
		o(x_1, x_2) &= \sigma\rbr{o_1w_{11}^{(2)} + o_2w_{12}^{(2)} + b_1^{(2)}}
	\end{align*}
	Then if the training data is $\{x_1^{(i)}, x_2^{(i)}, y^{(i)}\}_{i=1}^n$ the optimization problem for the binary cross-entropy loss is
	\[ \min_{w, b} -\frac{1}{n}\sum_{i=1}^n y^{(i)}\log o(x_1^{(i)}, x_2^{(i)}) + \rbr{1 - y^{(i)}}\log\rbr{1 - o(x_1^{(i)}, x_2^{(i)})} \]
	For the mean-square error:
	\[ \min_{w, b} \frac{1}{n}\sum_{i=1}^n (y^{(i)} - o(x_1^{(i)}, x_2^{(i)}))^2 \]
	The results of all the experiments are demonstrated on the plots below. We can see that the objective function is not far from beeing convex and
	that the cross-entropy produces more convex objective function for this neural network than mean-square loss. Also for this architecture there is 
	not much difference between full data and 10\% although it looks like the function for the full data is more convex. Probably more data still
	helps but not a lot.
\end{answer}

\begin{center}
	\includegraphics[width=0.9\textwidth]{full.png}
	\includegraphics[width=0.9\textwidth]{50.png}
	\includegraphics[width=0.9\textwidth]{25.png}
	\includegraphics[width=0.9\textwidth]{10.png}
\end{center}


\end{document} 
