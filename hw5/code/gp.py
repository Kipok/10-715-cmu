import numpy as np
import pandas as pd
from scipy.linalg import solve_triangular

def cov(x1, x2, sigma, h):
    return sigma * np.exp(-np.sum((x1[:,np.newaxis] - x2[np.newaxis, :]) ** 2, axis=-1) / (2 * h ** 2))

def compute_gp(x_train, y_train, x_test, y_test, sigma=None, h=None, gamma=None):
    if sigma is None:
        sigma = 3 * np.std(y_train, ddof=1)
    if h is None:
        h = 3 * np.linalg.norm(np.std(x_train, axis=0, ddof=1))
    if gamma is None:
        gamma = 0.01 * np.std(y_train, ddof=1)
    n = y_train.shape[0]

    K = cov(x_train, x_train, sigma, h)
    Ks = cov(x_train, x_test, sigma, h)
    Kt = cov(x_test, x_test, sigma, h)
    L = np.linalg.cholesky(K + gamma * np.eye(K.shape[0]))
    alpha = solve_triangular(L.T, solve_triangular(L, y_train, lower=True))
    mu = Ks.T.dot(alpha)
    v = solve_triangular(L, Ks, lower=True)
    Sigma = Kt - v.T.dot(v)
    logp = -0.5 * y_train.T.dot(alpha) - np.sum(np.log(np.diag(L))) - n / 2.0 * np.log(2.0 * np.pi)
    return Ks, mu, Sigma, logp

if __name__ == '__main__':
    data = pd.read_csv('data.csv', header=None)
    x_train = data.iloc[:,:-1].as_matrix()[:800]
    y_train = data.iloc[:,-1].as_matrix()[:800]
    x_test = data.iloc[:,:-1].as_matrix()[800:]
    y_test = data.iloc[:,-1].as_matrix()[800:]
    tr_mean = np.mean(y_train)

    Ks, mu, Sigma, logp = compute_gp(x_train, y_train - tr_mean, x_test, y_test)
    np.savetxt('kernel.csv', Ks, delimiter=',')
    np.savetxt('mean.csv', mu + tr_mean, delimiter=',')
    np.savetxt('variance.csv', Sigma, delimiter=',')

    sigmas = np.fromfile('factors.csv', sep='\n')
    logps = np.empty((sigmas.shape[0], sigmas.shape[0]))
    for i, v1 in enumerate(sigmas):
        for j, v2 in enumerate(sigmas):
            sigma = v1 * np.std(y_train, ddof=1)
            h = v2 * np.linalg.norm(np.std(x_train, axis=0, ddof=1))
            Ks, mu, Sigma, logps[i, j] = compute_gp(x_train, y_train - tr_mean, x_test, y_test, sigma, h)
    np.savetxt('likelihood.csv', logps, delimiter=',')

