\documentclass{article}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hwext}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{bbm}
\usepackage{float}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{mdwlist}
\usepackage{dirtree}
\usepackage{mdframed}
\usepackage[colorlinks=true]{hyperref}
\usepackage{geometry}
\usepackage{algorithm}
\usepackage{algpseudocode}
\geometry{margin=1in}
\geometry{headheight=2in}
\geometry{top=2in}
\usepackage{palatino}
%\renewcommand{\rmdefault}{palatino}
\usepackage{fancyhdr}
%\pagestyle{fancy}
\rhead{}
\lhead{}
\chead{%
  {\vbox{%
      \vspace{2mm}
      \large
      Machine Learning 10-715 \hfill
      Oct 27, 2016 \hfill \\
      \url{http://www.AdvIntroTo.ML} \hfill
      due on Nov 16, 2016
\\
      Carnegie Mellon University
      \\[2mm]
      Homework 5
    }
  }
}


\usepackage{paralist}

\usepackage{todonotes}
\setlength{\marginparwidth}{2.15cm}
\setlength{\parindent}{0pt}


\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{bchart}

\usetikzlibrary{calc,trees,positioning,arrows}
\usetikzlibrary{automata,positioning}
\usetikzlibrary{trees}
\usetikzlibrary{decorations.pathmorphing} % noisy shapes
\usetikzlibrary{fit}         % fitting shapes to coordinates
\usetikzlibrary{backgrounds} % drawing the background after the foreground
\usetikzlibrary{positioning}
\usetikzlibrary{shadows}
\usetikzlibrary{trees}
\usepgflibrary{shapes}
\usepackage{tikz-qtree}

\tikzstyle{observed}=[circle, thick, minimum size=8mm, draw=black!100, fill=black!20]
\tikzstyle{latent}=[circle, thick, minimum size=8mm, draw=black!80]
\tikzstyle{plate}=[rectangle, thick, inner sep=0.45cm, draw=black!100]
\tikzstyle{shadeplate}=[rectangle, thick, inner sep=0.4cm, draw=black!100]
\tikzstyle{table}=[circle,fill=blue!20,draw=black!100,inner sep=1pt, minimum size=30pt]
\tikzstyle{client}=[rectangle,fill=blue!20,draw=black!100,inner sep=1pt, minimum size=12pt]


\begin{document}
\pagestyle{fancy}

\section{Support Vector Regression [Vaishnavh; 25 pts]}
\begin{question}
\begin{enumerate}
\item (5pts) Rewrite the optimization problem with slack variables. Analogous to SVM, use
a parameter $C>0$ for weighing the slack variables in the objective function.\\
\textit{Hint: In this case, per data point two slack variables are needed.}
\end{enumerate}
\end{question}
\begin{answer}
	The original problem is equivalent to
	\begin{align*}
		&\min_{w}\ \frac{1}{2}\nbr{w}^2 + C\sum_{i=1}^nl_{0-\infty}(\abr{y_i - \inner{w}{x_i} - b}) \\
		&l_{0-\infty}(x) = \begin{cases} \infty, x > \epsilon \\ 0, x \le \epsilon \end{cases}
	\end{align*}
	In order to introduce slack variables let's replace $l_{0-\infty}$ with $l_{0-x}$:
	\begin{align*}
		&\min_{w}\ \frac{1}{2}\abr{w}^2 + C\sum_{i=1}^nl_{0-x}(\abr{y_i - \inner{w}{x_i} - b}) \\
		&l_{0-x}(x) = \begin{cases} x - \epsilon, x > \epsilon \\ 0, x \le \epsilon \end{cases}
	\end{align*}
	And this is equivalent to
	\begin{align*}
		\min_{w}\ &\frac{1}{2}\abr{w}^2 + C\sum_{i=1}^n\sbr{\max\rbr{y_i - \inner{w}{x_i} - b - \epsilon, 0} + \max\rbr{b + \inner{w}{x_i} - y_i
		- \epsilon, 0}}
		\Leftrightarrow \\
		\min_{w, \xi_1, \xi_2}\ &\frac{1}{2}\abr{w}^2 + C\sum_{i=1}^n\sbr{\xi_{1i} + \xi_{2i}} \\
		\text{s.t.}\ & y_i - \inner{w}{x_i} - b \le \epsilon + \xi_{1i}, \xi_{1i} \ge 0\ \forall i \\ 
		&y_i - \inner{w}{x_i} - b \ge -\epsilon - \xi_{2i}, \xi_{2i} \ge 0\ \forall i
	\end{align*}
\end{answer}

\begin{question}
\begin{enumerate}
\setcounter{enumi}{1}
\item (5pts) Write down Lagrangian and the KKT conditions.
\item (5pts) Formulate the dual problem.
\item (5pts) What are the values of dual variable if a data-point $(x_i, y_i)$ lies outside the $\epsilon
$ tube?
\item (5pts) Assuming we are given the solutions of the dual problem, recover the linear function $f$ we want, i.e. $w^*, b^*$.
\end{enumerate}
\end{question}
\begin{answer}
	\begin{enumerate}
    \setcounter{enumi}{1}
	\item
	\begin{align*}
		\min_{w, \xi_1, \xi_2}\ &\frac{1}{2}\abr{w}^2 + C\sum_{i=1}^n\sbr{\xi_{1i} + \xi_{2i}} \\
		\text{s.t.}\ & y_i - \inner{w}{x_i} - b \le \epsilon + \xi_{1i}, \xi_{1i} \ge 0\ \forall i \\ 
		&y_i - \inner{w}{x_i} - b \ge -\epsilon - \xi_{2i}, \xi_{2i} \ge 0\ \forall i \Rightarrow \\
		\Rightarrow L &= \frac{1}{2}\abr{w}^2 + C\sum_{i=1}^n\sbr{\xi_{i1} + \xi_{i2}} + 
		\sum_{i=1}^n[\lambda_i\rbr{y_i - \inner{w}{x_i} - b - \epsilon - \xi_{1i}} + \\ &+ \mu_i\rbr{-y_i + \inner{w}{x_i} + b - \epsilon - \xi_{2i}}
		- t_i\xi_{1i} - s_i\xi_{2i}]
	\end{align*}
	KKT conditions for optimal points $w, \xi_1, \xi_2, \lambda, \mu, t, s$:
	\begin{align*}
		&\partial(\frac{1}{2}\abr{w}^2 + C\sum_{i=1}^n\sbr{\xi_{1i} + \xi_{2i}} + 
		\sum_{i=1}^n[\lambda_i\rbr{y_i - \inner{w}{x_i} - b - \epsilon - \xi_{1i}} + \\ &+ \mu_i\rbr{-y_i + \inner{w}{x_i} + b - \epsilon - \xi_{2i}}
		- t_i\xi_{1i} - s_i\xi_{2i}]) = 0 \\
		&y_i - \inner{w}{x_i} - b \le \epsilon + \xi_{1i}, \xi_{1i} \ge 0,
		y_i - \inner{w}{x_i} - b \ge -\epsilon - \xi_{2i}, \xi_{2i} \ge 0\ \forall i \\
		&\lambda_i \ge 0, \mu_i \ge 0, s_i \ge 0, t_i \ge 0\ \forall i \\
		&\lambda_i[y_i - \inner{w}{x_i} - b - \epsilon - \xi_{1i}] = 0, \mu_i\rbr{-y_i + \inner{w}{x_i} + b - \epsilon - \xi_{2i}} = 0, 
		t_i\xi_{1i} = 0, s_i\xi_{2i} = 0
	\end{align*}
	\end{enumerate}
\end{answer}
\begin{answer}
	\begin{enumerate}
    \setcounter{enumi}{2}
	\item 
	To form the dual problem let's set the derivative of Lagrangian with respect to parameters to zero:
	\begin{align*}
		\frac{\partial L}{\partial w} &= w + \sum_{i=1}^n\rbr{\mu^Tx_i - \lambda^Tx_i} = 0 \Rightarrow w = \sum_{i=1}^n\rbr{\lambda - \mu}^Tx_i,
		\frac{\partial L}{\partial b} = \sum_{i=1}^n\rbr{\mu_i - \lambda_i} = 0 \\
		\frac{\partial L}{\partial \xi_1} &= C{\bf 1} - \lambda - t = 0,
		\frac{\partial L}{\partial \xi_2} = C{\bf 1} - \mu - s = 0 \Rightarrow \\
		\Rightarrow g(\lambda, \mu) &= \frac{1}{2}\inner{\sum_{i=1}^n(\lambda - \mu)^Tx_i}{\sum_{i=1}^n(\lambda - \mu)^Tx_i} + 
		\sum_{i=1}^n\sbr{\lambda_i\rbr{y_i - \inner{\sum_{j=1}^n(\lambda - \mu)^Tx_j}{x_i} - \epsilon }} \\ 
		&+ \sum_{i=1}^n\sbr{\mu_i\rbr{-y_i + \inner{\sum_{j=1}^n(\lambda - \mu)^Tx_j}{x_i} - \epsilon }} \rightarrow \max_{\lambda, \mu} \\
		\text{s.t.}\ \  &0 \le \lambda \le C, 0 \le \mu \le C, \sum_{i=1}^n(\mu_i - \lambda_i) = 0
	\end{align*}
	\item
	If $(x_i, y_i)$ lies outside the $\epsilon$ tube then $\xi_{1i} \ne 0$ or $\xi_{2i} \ne 0 \Rightarrow t_i = 0$ or  $s_i = 0 \Rightarrow \lambda_i = C$ or $\mu_i = C$.
	\item
	\[ w^* = \sum_{i=1}^n(\lambda^* - \mu^*)^Tx_i \]
	To compute bias term let's notice that from complimentary slackness conditions for any $\lambda_i \neq 0$ we get:
	\[ b^* = y_i - \inner{w^*}{x_i} - \epsilon - \xi_{1i}\]
	If $\lambda_i = 0$ we can compute the bias from $\mu_i$ (they cannot be both equal to zero at the same time).
	\end{enumerate}
\end{answer}

\newpage

\section{E-M for the Mixture of Experts Model [Manzil; 25pts]}
\begin{question}
\begin{enumerate}

\item (15 pts) Let's begin by showing that likelihood can be a lower bounded by:
\begin{equation}
\begin{aligned}
l(\mathbf{\theta;x,y}) &\geq
\sum_{n=1}^N \mathbb{E}_{p(z|y,x)}[\log p(z_{nk}|x_n; \gamma_k)] \\ 
&\qquad + \sum_{n=1}^N \mathbb{E}_{p(z|y,x)} [\log p(y_n|z_{nk},x_n;\theta_k, \sigma_k^2)]\\
\end{aligned}
\end{equation}

\item (10 pts) Derive the E step and M step update equations for $\theta_k$ where $k = 1, ..., K$ and finally summarise in the following template:
\paragraph*{E-Step}: Calculate for each $n=1,2,...,N$ and $k=1,2,...,K$:
\begin{equation}
\text{something here as function of } \theta^{(t)}
\end{equation}

\paragraph*{M-Step}: Get $\theta_k^{(t+1)}$  by solving the following linear system:
\begin{equation}
\begin{aligned}
\text{something else here}
\end{aligned}
\end{equation}

\end{enumerate}
\end{question}

\begin{answer}
	\begin{enumerate}
		\item
		For any density function $q(z)$:
		\begin{align*}
			l(\theta; x, y) &= \log p(y | x; \theta) = \int q(z) \log p(y | x; \theta)dz = 
			\int q(z) \log\frac{p(y | z, x; \theta)p(z | x; \theta)}{p(z | y, x; \theta)}dz = \\
			&= \int q(z) \log p(y | z, x; \theta)dz + \int q(z) \log p(z | x; \theta)dz - \int q(z) \log p(z | y, x; \theta)dz
		\end{align*}
		Setting $q(z) = p(z | y, x; \theta)$ we obtain (note that $\log p(z | y, x; \theta) \le 0$):
		\begin{align*}
			l(\theta; x, y) &= \mathbb{E}_{p(z | y, x)} \log p(y | z, x; \theta) + \mathbb{E}_{p(z | y, x)} \log p(z | x; \theta) - 
			\mathbb{E}_{p(y | z, x)}\log p(z | y, x; \theta) \ge \\
			&\ge \mathbb{E}_{p(z | y, x)} \log p(y | z, x; \theta) + \mathbb{E}_{p(z | y, x)} \log p(z | x; \theta) = \\
			&=  \sum_{n=1}^N \mathbb{E}_{p(z_n|y_n,x_n)} [\log p(y_n|z_{nk},x_n;\theta_k, \sigma_k^2)]
			+ \sum_{n=1}^N \mathbb{E}_{p(z_n|y_n,x_n)}[\log p(z_{nk}|x_n; \gamma_k)]
		\end{align*}
		\item
		During E step we need to compute $p(z | y, x)$:
		\begin{align*}
			p(z_n = k | y_n, x_n) \propto p(y_n | z_n = k , x_n) p(z_n = k | x_n) = \frac{1}{\sqrt{2\pi}\sigma_k}
			e^{-\frac{1}{2\sigma_k^2}(y_n - \theta_k^Tx_n)^2}\frac{e^{\gamma_kx_n}}{\sum_{k'=1}^Ke^{\gamma_{k'}x_n}}
		\end{align*}
		Since $p(z_n = k | y_n, x_n)$ is discrete distribution we can compute normalization constant by summing unnormalized $p(z = k | y, x)$ 
		over all $k$.

		During M step we need to compute the following expression (probability in the expectation is computed on the E step, meaning that it has 
		fixed parameters):
		\begin{align*} 
			&\argmax_{\theta, \sigma, \gamma}\sbr{\sum_{n=1}^N\mathbb{E}_{p(z_n | y_n, x_n)} \log p(y_n | z_n, x_n; \theta_k, \sigma_k) + 
			\mathbb{E}_{p(z_n | y_n, x_n)} \log p(z_n | x_n; \gamma_k)} = \\
			= &\argmax_{\theta, \sigma}\sum_{n=1}^N\mathbb{E}_z\sbr{\sum_{s=1}^Kz_{ns}\rbr{-\frac{1}{2}\log(2\pi) - 
			\log \sigma_s - \frac{1}{2\sigma_s^2}(y_n - \theta_s^Tx_n)^2}} + \\
			&+ \argmax_{\gamma}\sum_{n=1}^N\mathbb{E}_z\sbr{\sum_{s=1}^Kz_{ns}\rbr{\gamma_sx_n - \log\sbr{\sum_{k'=1}^Ke^{\gamma_{k'x_n}}}}}
		\end{align*}
		Since $z_{nk}$ follow Bernoulli distribution, $\mathbb{E}z_{nk} = p(z_{nk} = 1 | x_n, y_n) \equiv \pi_{nk}$. 
		Setting derivatives to zero we obtain
		\begin{align*}
			\frac{\partial f}{\partial \theta_k} &= \sum_{n=1}^N-\pi_{nk}\frac{1}{2\sigma^2_k}(\theta_k^Tx_n - y_n)x_n = 0 \Leftrightarrow
			\sbr{\sum_{n=1}^N\pi_{nk}(x_nx_n^T)}\theta_k = \sum_{n=1}^N\pi_{nk}y_nx_n \\
			\frac{\partial f}{\partial \sigma_k} &= \sum_{n=1}^N\pi_{nk}\sbr{-\frac{1}{\sigma_k} + \frac{1}{\sigma^3_k}(y_n - \theta_k^Tx_n)^2} = 0 
			\Leftrightarrow \sigma_k^2 = \frac{\sum_{n=1}^N\pi_{nk}(y_n - \theta_k^Tx_n)^2}{\sum_{n=1}^N\pi_{nk}} \\
			\frac{\partial f}{\partial \gamma_k} &= \sum_{n=1}^N\pi_{nk}\rbr{x_n - \frac{e^{\gamma_kx_n}}{\sum_{k'=1}^Ke^{\gamma_{k'}x_n}}x_n} = 0
			\Rightarrow \sum_{n=1}^N\pi_{nk}\frac{e^{\gamma_kx_n}}{\sum_{k'=1}^Ke^{\gamma_{k'}x_n}}x_n = \sum_{n=1}^N\pi_{nk}x_n
		\end{align*}
	\end{enumerate}
\end{answer}

\begin{answer}
	\paragraph*{E-Step}: Calculate for each $n=1,2,...,N$ and $k=1,2,...,K$:
	\begin{align*}
		p(z_n = k | y_n, x_n) &:= \frac{1}{\sqrt{2\pi}\sigma_k}e^{-\frac{1}{2\sigma_k^2}(y_n - x_n^T\theta_k^{(t)})^2}\frac{e^{\gamma_kx_n}}{\sum_{k'=1}^Ke^{\gamma_{k'}x_n}} \\ 
		p(z_n = k | y_n, x_n) &:= \frac{p(z_n = k | y_n, x_n)}{\sum_{t=1}^Kp(z_n = t | y_n, x_n)} \equiv \pi_{nk}
	\end{align*}
	
	\paragraph*{M-Step}: Get $\theta_k^{(t+1)}$  by solving the following linear system:
	\begin{equation}
	\begin{aligned}
		\sbr{\sum_{n=1}^N\pi_{nk}(x_nx_n^T)}\theta_k^{(t+1)} = \sum_{n=1}^N\pi_{nk}y_nx_n
	\end{aligned}
	\end{equation}
\end{answer}

\newpage

\section{Product of kernels [Manzil; 10pts]}

Let $k_1(x_1,x_2)$ and $k_2(x_1,x_2)$ be two kernel functions (i.e. symmetric and positive semi-definite functions). Prove that $k(x_1,x_2) = k_1(x_1,x_2)k_2(x_1,x_2)$ is also a kernel function.

\begin{answer}
	$k1(x, y)$ and $k2(x, y)$ are kernels, therefore
	\begin{align*}
		k_1(x, y) &= \inner{\phi(x)}{\phi(y)} = \phi(x)_1\phi(y)_1 + \dots + \phi(x)_n\phi(y)_n \\
		k_2(x, y) &= \inner{\psi(x)}{\psi(y)} = \psi(x)_1\psi(y)_1 + \dots + \psi(x)_m\psi(y)_m
	\end{align*}
	So
	\begin{align*}
		k_1(x, y)k_2(x, y) &= \sbr{\phi(x)_1\phi(y)_1 + \dots + \phi(x)_n\phi(y)_n}\sbr{\psi(x)_1\psi(y)_1 + \dots + \psi(x)_m\psi(y)_m} = \\
		&= \phi(x)_1\phi(y)_1\psi(x)_1\psi(y)_1 + \dots + \phi(x)_1\phi(y)_1\psi(x)_m\psi(y)_m + \dots + \\ &+ \phi(x)_n\phi(y)_n\psi(x)_1\psi(y)_1 + 
		\dots + \phi(x)_n\phi(y)_n\psi(x)_m\psi(y)_m = \inner{\tau(x)}{\tau(y)}
	\end{align*}
	Where
	\begin{align*}
		\tau(x) = \begin{bmatrix} \phi(x)_1\psi(x)_1 \\ \vdots \\ \phi(x)_1\psi(x)_m \\ \vdots \\ \phi(x)_n\psi(x)_1 \\ \vdots \\ \phi(x)_n\psi(x)_m 
		\end{bmatrix}
	\end{align*}
	If kernels are not finite, we can obtain the same result by considering $n \to \infty$, $m \to \infty$.
\end{answer}

\newpage

\section{Feedback (3 pts)} 
\begin{enumerate}
\item Approximately, how long did it take you to solve each question in the homework? [in hours]
\item Technically, did you find the homework too easy, just right or too hard?
\item With respect to time, did you find it too short, just right or too long?
\end{enumerate}
\begin{answer}
\begin{enumerate}
\item First question: 2 hours, second question: 3 hours, third question: 30 minutes, fourth question: 5 hours.
\item Too easy
\item Just right
\end{enumerate}
\end{answer}

\end{document} 
